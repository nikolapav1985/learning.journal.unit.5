Learning journal unit 5
-----------------------

- quack.py (print original and modified strings)
- str.py (more string examples, reverse, every ith character in range, every ith character)

Run examples
------------

- ./quack.py
- ./str.py

Test environment
----------------

OS lubuntu 16.04 lts 64 bit kernel version 4.13.0 python version 2.7.12
