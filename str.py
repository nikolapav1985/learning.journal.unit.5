#!/usr/bin/python

"""
FILE str.py
"""

def reverse(st):
    """
        method reverse

        reverse a string

        parameters

        st(string) a string to reverse

        return

        (string) reversed
    """
    return st[::-1]

def every_range(st,frm,to,i):
    """
        method every_range

        return every ith character in range

        parameters

        st(string) initial string
        frm(integer) initial index
        to(integer) last index
        i(integer) stride

        return

        (string) every ith character in range
    """
    return st[frm:to:i]

def every(st,i):
    """
        method every

        return every ith character

        parameters

        st(string) initial string
        i(integer) stride

        return

        (string) every ith character
    """
    return st[::i]

if __name__ == "__main__":
    st = "abcdefgh"
    frm=2
    to=5
    i=2
    print(st)
    print("---> reverse")
    print(reverse(st))
    print("---> every_range")
    print(every_range(st,frm,to,i))
    print("---> every")
    print(every(st,i))

"""
OUTPUT

abcdefgh
---> reverse
hgfedcba
---> every_range
ce
---> every
aceg
"""
