#!/usr/bin/python

"""
FILE quack.py
"""

def basic():
    """
        method basic

        basic unmodified method

        just print strings as given in learning journal 5
    """
    prefixes="JKLMNOPQ"
    suffix="ack"
    for letter in prefixes:
        print(letter + suffix)

def modified():
    """
        method modified

        a method used to print Ouack and Quack
    """
    prefixes="JKLMNOPQ"
    suffix="ack"
    for letter in prefixes:
        if letter == 'O' or letter == 'Q':
            print(letter + 'u' + suffix)
            continue
        print(letter + suffix)

if __name__ == "__main__":
    print("---> basic")
    basic()
    print("---> modified")
    modified()

"""
OUTPUT

---> basic
Jack
Kack
Lack
Mack
Nack
Oack
Pack
Qack
---> modified
Jack
Kack
Lack
Mack
Nack
Ouack
Pack
Quack
"""
